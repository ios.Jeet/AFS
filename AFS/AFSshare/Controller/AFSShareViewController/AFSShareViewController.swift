//
//  AFSShareViewController.swift
//  AFSshare
//
//  Created by Jitendra Kumar on 09/07/20.
//  Copyright © 2020 Harsh Rajput. All rights reserved.
//

import UIKit
import Social
import Alamofire
import MobileCoreServices

open class AFSShareViewController: UIViewController {
    //MARK:- Properties
    @IBOutlet fileprivate  var scrollView: UIScrollView!
    @IBOutlet fileprivate  var collectionHeight: NSLayoutConstraint!
    @IBOutlet fileprivate  var emailTF: JKTextField!
    @IBOutlet fileprivate  var orchardTF: JKTextField!
    @IBOutlet fileprivate  var blockTF: JKTextField!
    @IBOutlet fileprivate  var rowTF: JKTextField!
    @IBOutlet fileprivate  var textView: JKTextView!
    @IBOutlet fileprivate  var collectionView: UICollectionView!
    @IBOutlet fileprivate  var postBtn:UIButton!
    @IBOutlet fileprivate  var cancelBtn:UIButton!
    @IBOutlet fileprivate  var imageCountlbl:UILabel!
    @IBOutlet fileprivate  var textCountlbl:UILabel!
    @IBOutlet fileprivate  var indicatorView:UIActivityIndicatorView!
    
    fileprivate var imagelist : [AFSImage] = []
    fileprivate var imageAttachments: [NSItemProvider] = []
    fileprivate var index:Int = -1
    
    fileprivate var contentText:String{
        return textView?.text ?? ""
    }
    fileprivate var orchardText:String{
        return orchardTF?.text ?? ""
    }
    fileprivate var blockText:String{
        return blockTF?.text ?? ""
    }
    fileprivate var emailText:String{
        return emailTF.text ?? ""
    }
    
    fileprivate var rowText:String{
        return rowTF?.text ?? ""
    }
    
    
    fileprivate func isContentValid() {
        self.imageCountlbl.text = "\(self.imagelist.count) Images"
        self.textCountlbl.text = "\(self.textView.maxLength - self.contentText.count )"
        if  imagelist.count>0,!emailText.isEmpty,emailText.isEmail{
            self.postBtn.isEnabled = true
        }else{
            self.postBtn.isEnabled = false
        }
        
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOSApplicationExtension 13.0, *) {
            indicatorView.style = .medium
            view.overrideUserInterfaceStyle = .light
        } else {
            indicatorView.style = .gray
        }
        //textView.text = "AFS Image Set"
        
        indicatorView.hidesWhenStopped = true
      
        if #available(iOSApplicationExtension 13.0, *) {
            self.view.backgroundColor = .systemGray6
        } else {
            self.view.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.968627451, alpha: 1)
        }
        textView.didChangeText = {(text,_) in
            async {
                self.isContentValid()
            }
        }
        if let email = kUserData?.email {
            emailTF.text = email
        }
        
        emailTF.didChangeAction {tf in
            self.isContentValid()
        }
        orchardTF.didChangeAction {tf in
            self.isContentValid()
        }
        blockTF.didChangeAction {tf in
            self.isContentValid()
        }
        rowTF.didChangeAction {tf in
            self.isContentValid()
        }
        
        
        FileManager.deleteDirectory()
        FileManager.createDirectory()
        
        findImages()
        isContentValid()
        
    }
    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionHeight.constant = collectionView.contentSize.height
        self.scrollView.layoutIfNeeded()
    }
    //MARK:- onCancel
    @IBAction private func onCancel(_ sender: Any) {
        FileManager.deleteDirectory()
        if self.imageAttachments.count>0 {
            self.imageAttachments.removeAll()
        }
        self.extensionContext?.cancelRequest(withError: AFSError.cancelled)
    }
    //MARK:- onPost
    @IBAction private func onPost(_ sender: Any) {
        self.view.endEditing(true)
        self.index = 0
        FileManager.deleteDirectory()
        self.uploadMultipart()
    }
    //MARK:- onOrchard
    @IBAction private func onOrchard(_ sender: Any) {
        self.view.endEditing(true)
        let viewModel  = PickerViewModel.shared
        viewModel.removeAll()
        if emailText.isEmpty {
            showAlert(message: "Please enter your email")
        }else{
            self.startAnimating()
            viewModel.getDropdownData(email: emailText) { errorMessage in
                async {
                    self.stopAnimating()
                    if let message = errorMessage{
                        self.showAlert(message:message)
                    }else{
                        self.showDropdown(items: PickerViewModel.shared.dropdownList, sender: sender, configure: { (cell, item) in
                            cell.textLabel?.text = item.orchard
                        }, didSelect: {item in
                            self.orchardTF.text = item.orchard
                            viewModel.set(item.blocks)
                             self.isContentValid()
                        })
                    }
                    
                }
            }
        }
        
    }
    //MARK:- onBlock
    @IBAction private func onBlock(_ sender: Any) {
        self.view.endEditing(true)
        let viewModel  = PickerViewModel.shared
        if  orchardText.isEmpty {
            showAlert(message: "Please select Orchard value")
        }else{
            self.showDropdown(items: viewModel.blocklist, sender: sender, configure: { (cell, item) in
                cell.textLabel?.text = item
            }, didSelect: {item in
                self.blockTF.text = item
                self.isContentValid()
                
            })
        }
        
    }
    
    open override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("Get Memory issues")
    }
    
    
}
extension AFSShareViewController{
    //MARK:- findImages
    fileprivate func findImages(){
        if self.imagelist.count>0 {
            self.imagelist.removeAll()
        }
        guard let context = self.extensionContext else{
            self.dismiss(animated: true, completion: nil)
            return
            
        }
        
        guard let inputItems = context.inputItems as? [NSExtensionItem], let content = inputItems.first,let attachments = content.attachments else {
            context.cancelRequest(withError: AFSError.noAttachment)
            return
        }
        self.startAnimating()
        imageAttachments = attachments.filter({ $0.hasItemConformingToTypeIdentifier(.image)})
        
        if self.imageAttachments.count>0 {
            imageAttachments.fetchBatchImages(forTypeIdentifier: .image,request: .previewImage,partialFetchHandler: { (afsImage, index) in
                DispatchQueue.main.async {
                    guard let afsImage = afsImage else {
                        context.cancelRequest(withError: AFSError.noContentType(.image))
                        return
                        
                    }
                    self.imagelist.append(afsImage)
                    let progress = Double(((index + 1) * 100) / self.imageAttachments.count)
                    print(progress)
                }
            }, completion: {
                print("Finished fetching avatars!")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                    async {
                        self.stopAnimating()
                        self.collectionView.reloadData()
                        self.isContentValid()
                        self.scrollView.layoutIfNeeded()
                    }
                    
                })
            })
            
            
            
        }
        
        
        
    }
    
    
    
    //MARK:- upload Images
    fileprivate var cell:ImageGridCell?{
        if self.index<self.imagelist.count{
            return collectionView.cellForItem(at: .init(row: self.index, section: 0)) as? ImageGridCell
        }else{
            return nil
        }
        
    }
    fileprivate func startAnimating(){
        postBtn.isHidden = true
        cancelBtn.isEnabled = false
        indicatorView.startAnimating()
    }
    fileprivate func stopAnimating(){
        indicatorView.stopAnimating()
        self.cancelBtn.isEnabled = true
        self.postBtn.isHidden = false
    }
    //MARK:- uploadMultipart
    fileprivate func uploadMultipart(){
        if kUserData == nil {
            let udid = UIDevice.current.identifierForVendor?.uuidString ?? ""
            let emailText = emailTF.text ?? ""
            kUserData = .init(email: emailText, uuidString: udid)
        }else if let userData = kUserData,  userData.email != emailTF.text{
            kUserData?.email = emailTF.text ?? ""
        }
        
        guard CBServer.shared.isConnected,self.imagelist.count>0,let userData = kUserData,   !userData.email.isEmpty, !userData.uuidString.isEmpty, imagelist.count>index else{
            self.stopAnimating()
            return
            
        }
        
        self.startAnimating()
        let completion  = { (_ result:Swift.Result<Int, Error>)  in
            async {
                switch result{
                case .success(let vl):
                    print(String(describing: vl))
                    self.cell?.progress = 0.0
                    if self.index == self.imagelist.count-1 {
                        self.stopAnimating()
                        let top:CGPoint  = .init(x: 0, y: 0)
                        self.scrollView.setContentOffset(top, animated: true)
                        self.showAlert(message: "\(self.imagelist.count) images uploaded to AFS") {_ in
                            self.extensionContext?.completeRequest(returningItems: [], completionHandler: { success in
                                self.index = -1
                                self.imagelist.removeAll()
                                self.collectionView.reloadData()
                                if self.imageAttachments.count>0 {
                                    self.imageAttachments.removeAll()
                                }
                                
                            })
                        }
                        
                    }else if self.index<self.imagelist.count-1{
                        self.index += 1
                        self.uploadMultipart()
                        self.collectionView.scrollToItem(at: .init(row: self.index, section: 0), at: .bottom, animated: true)
                        self.collectionView.reloadItems(at: [.init(row: self.index, section: 0)])
                        if let temp  = self.collectionView.cellForItem(at: .init(row: self.index, section: 0)){
                            let bottom:CGPoint  = .init(x: 0, y: temp.center.y)
                            self.scrollView.setContentOffset(bottom, animated: true)
                        }
                        
                    }
                case .failure(let error):
                    debugPrint(error.localizedDescription)
                    self.indicatorView.stopAnimating()
                    self.showAlertAction(message: "We're having trouble with our server.\nPlease try again.", otherTitle: "Try Again") { indx in
                        if indx == 2{
                            self.cell?.progress = 0.0
                            self.uploadMultipart()
                        }else{
                            self.cell?.progress = 0.0
                            self.extensionContext?.cancelRequest(withError: error)
                        }
                    }
                    
                    
                    
                }
            }
        }
        let uploadProgress = {(_ progress:Progress) in
            // let totalSize = ByteCountFormatter.string(fromByteCount: progress.totalUnitCount, countStyle: .file)
            self.cell?.progress = progress.fractionCompleted
            print(progress)
        }
        
        CBServer.shared.upload(.default, .data([self.imagelist[index].data]), description:  self.contentText,email: userData.email,numOfImages:imagelist.count,orchard: orchardText,block: blockText,row: rowText,UDIDString: userData.uuidString, completion: completion, uploadProgress: uploadProgress)
        
        
        
    }
    
    
    
}
extension AFSShareViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagelist.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageGridCell", for: indexPath) as! ImageGridCell
        cell.image = self.imagelist[indexPath.row].image
        return cell
    }
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  0
        let collectionViewSize = (collectionView.frame.size.width - padding)/2
        return .init(width: collectionViewSize, height: collectionViewSize)
    }
}




