//
//  UITableView+Ex.swift
//  Pivot
//
//  Created by Jitendra Kumar on 28/04/20.
//  Copyright © 2020 Jitendra Kumar. All rights reserved.
//

import UIKit

struct Platform {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
        isSim = true
        #endif
        return isSim
    }()
    static var isPhone:Bool {
        return UIDevice.current.userInterfaceIdiom == .phone ? true :false
    }
    static var isPad:Bool {
        return UIDevice.current.userInterfaceIdiom == .pad ? true :false
    }
}

extension UITableView{
    func updateHeaderHeight(){
        guard let headerView = self.tableHeaderView else {return}
        // The table view header is created with the frame size set in
        // the Storyboard. Calculate the new size and reset the header
        // view to trigger the layout.
        // Calculate the minimum height of the header view that allows
        // the text label to fit its preferred width.
        let size = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            
            // Need to set the header view property of the table view
            // to trigger the new layout. Be careful to only do this
            // once when the height changes or we get stuck in a layout loop.
            self.tableHeaderView = headerView
            
            // Now that the table view header is sized correctly have
            // the table view redo its layout so that the cells are
            // correcly positioned for the new header size.
            // This only seems to be necessary on iOS 9.
            self.layoutIfNeeded()
        }
        
    }
    func updateFooterHeight(){
        guard let footerView = self.tableFooterView else {return}
        // The table view footer is created with the frame size set in
        // the Storyboard. Calculate the new size and reset the header
        // view to trigger the layout.
        // Calculate the minimum height of the header view that allows
        // the text label to fit its preferred width.
        let size = footerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        if footerView.frame.size.height != size.height {
            footerView.frame.size.height = size.height
            
            // Need to set the header view property of the table view
            // to trigger the new layout. Be careful to only do this
            // once when the height changes or we get stuck in a layout loop.
            self.tableFooterView = footerView
            
            // Now that the table view header is sized correctly have
            // the table view redo its layout so that the cells are
            // correcly positioned for the new header size.
            // This only seems to be necessary on iOS 9.
            self.layoutIfNeeded()
        }
        
    }
    
    func setContentInset(_ insets:UIEdgeInsets){
        self.contentInset = insets
    }
    func setScrollIndicatorInsets(_ insets:UIEdgeInsets){
        self.scrollIndicatorInsets = insets
    }
    var indexPathForLastRow:IndexPath?{
        let section = max(numberOfSections - 1, 0)
        let row = max(numberOfRows(inSection: section) - 1, 0)
        if row>0{
            return IndexPath(row: row, section: section)
        }else{
            return nil
        }
        
        
    }
    
    func register<T: UITableViewCell>(_: T.Type, reuseIdentifier: String? = nil) {
        self.register(T.self, forCellReuseIdentifier: reuseIdentifier ?? String(describing: T.self))
    }
    
    func dequeue<T: UITableViewCell>(_: T.Type,reuseIdentifier: String? = nil, for indexPath: IndexPath) -> T {
        guard let cell = dequeueCell(reuseIdentifier:reuseIdentifier ?? String(describing: T.self),for: indexPath) as? T
            else { fatalError("Could not deque cell with type \(T.self)") }
        
        return cell
    }
    func dequeue<T: UITableViewCell>(_: T.Type,reuseIdentifier: String? = nil) -> T? {
        guard let cell = dequeueCell(reuseIdentifier:reuseIdentifier ?? String(describing: T.self)) as? T
            else { fatalError("Could not deque cell with type \(T.self)") }
        
        return cell
    }
    func dequeueCell(reuseIdentifier identifier: String) -> UITableViewCell? {
        return dequeueReusableCell(withIdentifier: identifier)
    }
    func dequeueCell(reuseIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
        return dequeueReusableCell(
            withIdentifier: identifier,
            for: indexPath
        )
    }
    func multilineCell<T:UITableViewCell>(cell:T, for height:CGFloat,minHeight:CGFloat = 60){
        
        if height>0, minHeight>0, height != minHeight {
            UIView.setAnimationsEnabled(false)
            self.beginUpdates()
            self.endUpdates()
            UIView.setAnimationsEnabled(true)
            if let thisIndexPath = self.indexPath(for: cell) {
                self.scrollToRow(at: thisIndexPath, at: .bottom, animated: false)
            }
        }
    }
    
    
}
extension UITableViewCell{
    var identifier:String{
        return String(describing: self)
    }
}

extension UICollectionView{
    
    enum ElementKindSection:Int{
        case header
        case footer
        var kind:String{
            switch self {
            case .header:
                return UICollectionView.elementKindSectionHeader
            default:
                return UICollectionView.elementKindSectionFooter
                
            }
        }
    }
    //MARK:- forCell
    func register<T: UICollectionViewCell>(_: T.Type, reuseIdentifier: String? = nil) {
        self.register(T.self, forCellWithReuseIdentifier: reuseIdentifier ?? String(describing: T.self))
    }
    
    func dequeue<T: UICollectionViewCell>(_: T.Type,reuseIdentifier: String? = nil, for indexPath: IndexPath) -> T {
        guard let cell = dequeueCell(reuseIdentifier:reuseIdentifier ?? String(describing: T.self),for: indexPath) as? T
            else { fatalError("Could not dequeue cell with type \(T.self)") }
        
        return cell
    }
    
    func dequeueCell(reuseIdentifier identifier: String, for indexPath: IndexPath) -> UICollectionViewCell {
        return dequeueReusableCell(
            withReuseIdentifier: identifier,
            for: indexPath
        )
    }
    //MARK:- forSupplementaryViewOfKind
    func register<T: UICollectionReusableView>(_: T.Type,forSupplementaryViewOfKind element: ElementKindSection, reuseIdentifier: String? = nil) {
        self.register(T.self, forSupplementaryViewOfKind: element.kind, withReuseIdentifier: reuseIdentifier ?? String(describing: T.self))
    }
    
    func dequeue<T:UICollectionReusableView>(_: T.Type,reuseIdentifier: String? = nil,ofKind element: ElementKindSection, for indexPath: IndexPath) -> T {
        guard let cell = dequeueSupplementaryView(ofKind: element, withReuseIdentifier: reuseIdentifier ?? String(describing: T.self), for: indexPath) as? T
            else { fatalError("Could not dequeue ReusableView> with type \(T.self)") }
        return cell
    }
    func dequeueSupplementaryView(ofKind element: ElementKindSection,withReuseIdentifier identifier: String, for indexPath: IndexPath) -> UICollectionReusableView {
        return dequeueReusableSupplementaryView(ofKind:element.kind, withReuseIdentifier: identifier, for: indexPath)
        
    }
    
}
extension UICollectionViewCell{
    var identifier:String{
        return String(describing: self)
    }
}

