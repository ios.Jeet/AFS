//
//  AFSDropdownController.swift
//  AFSshare
//
//  Created by Jitendra Kumar on 06/08/20.
//  Copyright © 2020 Jitendra Kumar. All rights reserved.
//

import UIKit

class AFSDropdownController<Item, Cell: UITableViewCell>:UITableViewController{
    var items: [Item] = []
    let reuseIdentifier = "Cell"
    var configure: (Cell, Item) -> ()
    var didSelect: (Item)->()
    
    init(style:UITableView.Style = .plain,items:[Item],configure:@escaping(Cell,Item)->Void,didSelect:@escaping(Item)->Void) {
        self.items = items
        self.configure = configure
        self.didSelect = didSelect
        super.init(style: style)
        //self.tableView.register(Cell.self, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        tableView.register(Cell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        didUpdatePreferredContentHieght()
    }
    fileprivate func didUpdatePreferredContentHieght(){
        self.tableView.reloadData()
        UIView.animate(withDuration: 0.3) {
            self.preferredContentSize.height = self.tableView.contentSize.height
        }
        tableView.isScrollEnabled =  self.view.bounds.height<tableView.contentSize.height ? true:false
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(Cell.self, reuseIdentifier: reuseIdentifier, for: indexPath)
        //let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! Cell
        let item = items[indexPath.row]
        configure(cell, item)
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            let item = self.items[indexPath.row]
            self.didSelect(item)
        }
    }
    
}




extension UIViewController:UIPopoverPresentationControllerDelegate{
    
    func showDropdown<Item, Cell: UITableViewCell>(style:UITableView.Style = .plain,items:[Item],sender:Any,configure:@escaping(Cell,Item)->Void,didSelect:@escaping(Item)->Void){
        let pickerController =  AFSDropdownController(style: style, items: items, configure: configure, didSelect: didSelect)
        pickerController.popoverPresentation()
        if let popoverController = pickerController.popoverPresentationController {
            popoverController.permittedArrowDirections = .any
            if let sourceView = sender as? UIBarButtonItem {
                popoverController.barButtonItem = sourceView
            }else if let source = sender as? UIView{
                popoverController.sourceView = source
                popoverController.sourceRect = source.bounds
                pickerController.preferredContentSize = source.bounds.size
            }
            popoverController.delegate = self
            
        }
        self.present(pickerController, animated: true, completion: nil)
        
    }
    
    public func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        if Platform.isPhone {
            return .none
        }else{
            return .popover
        }
    }
}
