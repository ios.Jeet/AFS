//
//  AFSImage.swift
//  AFS
//
//  Created by Jitendra Kumar on 23/07/20.
//  Copyright © 2020 Harsh Rajput. All rights reserved.
//

import Foundation
import UIKit

open class AFSImage:NSObject{
    open var image:UIImage
    open var data:Data?
    open var fileDirectioryURL:URL?
    init(image:UIImage,data:Data? = nil, fileDirectioryURL  url:URL? = nil) {
        self.image = image
        self.data = data
        self.fileDirectioryURL = url
        
    }
   
}
