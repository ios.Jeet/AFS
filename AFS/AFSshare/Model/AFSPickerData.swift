//
//  AFSPickerData.swift
//  AFSshare
//
//  Created by Jitendra Kumar on 06/08/20.
//  Copyright © 2020 Harsh Rajput. All rights reserved.
//

import Foundation



public struct AFSPickerResponse:Mappable,Hashable {
    
    public var meta:[AFSPickerData]
    enum CodingKeys:String,CodingKey {
        case meta
    }
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        meta = try container.decodeIfPresent([AFSPickerData].self, forKey: .meta) ?? []
        
    }
}
public struct AFSPickerData:Mappable,Hashable {
    public var orchard:String
    public var blocks:[String]
    enum CodingKeys:String,CodingKey {
        case orchard,blocks
    }
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        orchard = try container.decodeIfPresent(String.self, forKey: .orchard) ?? ""
        blocks = try container.decodeIfPresent([String].self, forKey: .blocks) ?? []
    }
}
