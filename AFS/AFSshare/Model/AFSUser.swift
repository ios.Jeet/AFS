//
//  AFSUser.swift
//  AFSshare
//
//  Created by Jitendra Kumar on 06/08/20.
//  Copyright © 2020 Harsh Rajput. All rights reserved.
//

import Foundation

public struct AFSUser:Mappable,Hashable {
    public var email:String
    public var uuidString:String
    enum CodingKeys:String,CodingKey {
        case uuidString = "udid"
        case email = "email"
    }
}
let kUserDataKey   = "UserDataKey"
 var kUserData:AFSUser?{
    set{
        guard let vl = newValue else {
            CBKeychain.removeValue(forKey: kUserDataKey)
            return
        }
        _ = CBKeychain.set(encoder: vl, forKey: kUserDataKey)
    }
    get{
        return CBKeychain.get(decoder: AFSUser.self, forKey: kUserDataKey)
    }
    
}
