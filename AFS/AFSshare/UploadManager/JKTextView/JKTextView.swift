//
//  JKTextView.swift
//  MOM
//
//  Created by RAWAT on 17/10/17.
//  Copyright © 2017 360itpro. All rights reserved.
//

import UIKit


@IBDesignable
open class JKTextView: UITextView {
    
    
    private let placeholderLabel: UILabel = UILabel()
    private var placeholderLabelConstraints = [NSLayoutConstraint]()
    var didChangeText :((_ text:String,  _ height:CGFloat)->Void)?
    var didEndEditingText:((_ text:String)->Void)?
    @IBInspectable open var placeholder: String = "" {
        didSet {
            placeholderLabel.text = placeholder
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable open var placeholderColor: UIColor = .lightGray {
        didSet {
            placeholderLabel.textColor = placeholderColor
            self.setNeedsLayout()
        }
    }
    
    
    // Maximum length of text. 0 means no limit.
    @IBInspectable open var maxLength: Int = 0{
        didSet{
            self.textContainer.maximumNumberOfLines = maxLength
            self.setNeedsLayout()
        }
    }
    
    
    @IBInspectable public var cornerRadius: CGFloat = 2.5 {
        didSet {
            layer.cornerRadius = cornerRadius
            self.setNeedsLayout()
        }
    }
    
    
    @IBInspectable public var borderColor: UIColor =  UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
            self.setNeedsLayout()
        }
    }
    @IBInspectable public var borderWidth: CGFloat =  0 {
        didSet {
            layer.borderWidth = borderWidth
            self.setNeedsLayout()
            
        }
    }
    @IBInspectable public var masksToBounds : Bool = false{
        didSet{
            layer.masksToBounds = masksToBounds
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable public var clipsToBound : Bool = false{
        didSet{
            self.clipsToBounds = clipsToBound
            self.setNeedsLayout()
        }
    }
    
    
    override open var font: UIFont! {
        didSet {
            if placeholderFont == nil {
                placeholderLabel.font = font
            }
        }
    }
    
    open var placeholderFont: UIFont? {
        didSet {
            let font = (placeholderFont != nil) ? placeholderFont : self.font
            placeholderLabel.font = font
            self.setNeedsLayout()
        }
    }
    
    override open var textAlignment: NSTextAlignment {
        didSet {
            placeholderLabel.textAlignment = textAlignment
            self.setNeedsLayout()
        }
    }
    
    override open var text: String! {
        didSet {
            // textDidChange()
            placeholderLabel.isHidden = !text.isEmpty
            self.setNeedsLayout()
        }
    }
    
    override open var attributedText: NSAttributedString! {
        didSet {
            //textDidChange()
            placeholderLabel.isHidden = attributedText.length>0
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable public var bottomBorderEnabled: Bool = true {
        didSet {
            bottomBorderLayer?.removeFromSuperlayer()
            bottomBorderLayer = nil
            if bottomBorderEnabled {
                bottomBorderLayer = CALayer()
                bottomBorderLayer?.frame = CGRect(x: 0, y: layer.bounds.height - 1, width: bounds.width, height: 1)
                bottomBorderLayer?.backgroundColor = bottomBorderColor.cgColor
                layer.addSublayer(bottomBorderLayer!)
            }
            self.setNeedsLayout()
        }
    }
    @IBInspectable public var trimWhiteSpaceWhenEndEditing:Bool = false
    @IBInspectable public var bottomBorderWidth: CGFloat = 1.0
    @IBInspectable public var bottomBorderColor: UIColor = .gray
    @IBInspectable public var bottomBorderHighlightWidth: CGFloat = 1.75
    fileprivate(set) var bottomBorderLayer: CALayer?
    
    
    override open var textContainerInset: UIEdgeInsets {
        didSet {
            updateConstraintsForPlaceholderLabel()
            self.setNeedsLayout()
        }
    }
    
    override public init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(JKTextView.textDidChange(_:)),
                                               name: UITextView.textDidChangeNotification,
                                               object: self)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(JKTextView.textDidEndEditing(_:)),
                                               name: UITextView.textDidEndEditingNotification,
                                               object: self)
        
        placeholderLabel.font = font
        placeholderLabel.textColor = placeholderColor
        placeholderLabel.textAlignment = textAlignment
        placeholderLabel.text = placeholder
        placeholderLabel.numberOfLines = 0
        placeholderLabel.backgroundColor = UIColor.clear
        placeholderLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(placeholderLabel)
        updateConstraintsForPlaceholderLabel()
    }
    
    private func updateConstraintsForPlaceholderLabel() {
        var newConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-(\(textContainerInset.left + textContainer.lineFragmentPadding))-[placeholder]",
            options: [],
            metrics: nil,
            views: ["placeholder": placeholderLabel])
        newConstraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-(\(textContainerInset.top))-[placeholder]",
            options: [],
            metrics: nil,
            views: ["placeholder": placeholderLabel])
        newConstraints.append(NSLayoutConstraint(
            item: placeholderLabel,
            attribute: .width,
            relatedBy: .equal,
            toItem: self,
            attribute: .width,
            multiplier: 1.0,
            constant: -(textContainerInset.left + textContainerInset.right + textContainer.lineFragmentPadding * 2.0)
        ))
        removeConstraints(placeholderLabelConstraints)
        addConstraints(newConstraints)
        placeholderLabelConstraints = newConstraints
    }
    //MARK:- Notification Observer Methods
    // Limit the length of text
    @objc private func textDidChange(_ notification: Notification) {
        placeholderLabel.isHidden = !text.isEmpty
        guard let sender = notification.object as? JKTextView, sender == self  else { return}
        if maxLength > 0 && text.count > maxLength {
            let endIndex = text.index(text.startIndex, offsetBy: maxLength)
            text = String(text[..<endIndex])
            undoManager?.removeAllActions()
        }
        setNeedsDisplay()
        didChangeText?(self.text, self.text.height(with: self.bounds.width, font: self.font))
        
        
        
    }
    //While Call function when Keyboard resign
    @objc private func textDidEndEditing(_ notification: Notification){
        guard let sender = notification.object as? JKTextView, sender == self  else { return}
        if trimWhiteSpaceWhenEndEditing {
            text = text?.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        setNeedsDisplay()
        didEndEditingText?(text)
        
    }
    open override func layoutSubviews() {
        super.layoutSubviews()
        placeholderLabel.preferredMaxLayoutWidth = textContainer.size.width - textContainer.lineFragmentPadding * 2.0
        bottomBorderLayer?.backgroundColor = isFirstResponder ? tintColor.cgColor : bottomBorderColor.cgColor
        let borderWidth = isFirstResponder ? bottomBorderHighlightWidth : bottomBorderWidth
        bottomBorderLayer?.frame = CGRect(x: 0, y: layer.bounds.height - borderWidth, width: layer.bounds.width, height: borderWidth)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        
    }
    
}
extension UITextView{
    var textHeight:CGFloat{
        guard let font = self.font else { return 0 }
        return text.height(with: self.bounds.width, font: font)
    }
    var textWidth:CGFloat{
        guard let font = self.font else { return 0 }
        return text.width(with: self.bounds.height, font: font)
    }
    typealias Match = (prefix: String, word: String, range: NSRange)
    /// Scroll to the bottom of text view
    public func scrollToBottom() {
        let range = NSMakeRange(text.count - 1, 1)
        scrollRangeToVisible(range)
    }
    
    /// Scroll to the top of text view
    public func scrollToTop() {
        let range = NSMakeRange(0, 1)
        scrollRangeToVisible(range)
    }
    
    
    func find(prefixes: Set<String>, with delimiterSet: CharacterSet) -> Match? {
        guard prefixes.count > 0 else { return nil }
        
        for prefix in prefixes {
            if let match = find(prefix: prefix, with: delimiterSet) {
                return match
            }
        }
        return nil
    }
    
    func find(prefix: String, with delimiterSet: CharacterSet) -> Match? {
        guard !prefix.isEmpty else { return nil }
        guard let caretRange = self.caretRange else { return nil }
        guard let cursorRange = Range(caretRange, in: text) else { return nil }
        
        let leadingText = text[..<cursorRange.upperBound]
        var prefixStartIndex: String.Index!
        for (i, char) in prefix.enumerated() {
            guard let index = leadingText.lastIndex(of: char) else { return nil }
            if i == 0 {
                prefixStartIndex = index
            } else if index.utf16Offset(in: leadingText) == prefixStartIndex.utf16Offset(in: leadingText) + 1 {
                prefixStartIndex = index
            } else {
                return nil
            }
        }
        
        let wordRange = prefixStartIndex..<cursorRange.upperBound
        let word = leadingText[wordRange]
        
        let location = wordRange.lowerBound.utf16Offset(in: leadingText)
        let length = wordRange.upperBound.utf16Offset(in: word) - location
        let range = NSRange(location: location, length: length)
        
        return (String(prefix), String(word), range)
    }
    
    var caretRange: NSRange? {
        guard let selectedRange = self.selectedTextRange else { return nil }
        return NSRange(
            location: offset(from: beginningOfDocument, to: selectedRange.start),
            length: offset(from: selectedRange.start, to: selectedRange.end)
        )
    }
}
