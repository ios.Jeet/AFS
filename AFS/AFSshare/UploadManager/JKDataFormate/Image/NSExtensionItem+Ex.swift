//
//  NSExtensionItem+Ex.swift
//  AFSshare
//
//  Created by Jitendra Kumar on 28/07/20.
//  Copyright © 2020 Harsh Rajput. All rights reserved.
//

import Foundation
import UIKit
extension Array where Element:NSItemProvider{
    
    func fetchBatchImages(forTypeIdentifier typeIdentifier: UTI,
                          request:NSItemProvider.RequestType,
                          partialFetchHandler: @escaping (_ imageData: AFSImage?, _ index: Int) -> Void,
                          completion: @escaping () -> Void){
        //FileManager.createDirectory()
        performBatchImageFetching(forTypeIdentifier:typeIdentifier,using: self, currentImageIndex: 0,request: request, partialFetchHandler: { (imageData, index) in
            partialFetchHandler(imageData, index)
        }) {
            
            completion()
        }
        
    }
    
    //MARK:-  BatchImageFetching USING Data List
    private func performBatchImageFetching(forTypeIdentifier typeIdentifier: UTI,using datas: [NSItemProvider],
                                           currentImageIndex: Int,
                                           request:NSItemProvider.RequestType,
                                           partialFetchHandler: @escaping (_ imageData: AFSImage?, _ index: Int) -> Void,
                                           completion: @escaping () -> Void) {
        
        
        
        guard currentImageIndex < datas.count else {
            completion()
            return
        }
        datas[currentImageIndex].fetchImageData(forTypeIdentifier: typeIdentifier,request: request) {imageData  in
            partialFetchHandler(imageData, currentImageIndex)
            self.performBatchImageFetching(forTypeIdentifier: typeIdentifier, using: datas, currentImageIndex: currentImageIndex + 1,request: request, partialFetchHandler: partialFetchHandler){
                completion()
                
            }
        }
        
    }
}
extension NSExtensionItem{
    
    func fetchBatchImages(forTypeIdentifier typeIdentifier: UTI,request:NSItemProvider.RequestType = .dataRepresent,
                          partialFetchHandler: @escaping (_ imageData: AFSImage?, _ index: Int) -> Void,
                          completion: @escaping () -> Void){
        guard let attachments = self.attachments?.filter({$0.hasItemConformingToTypeIdentifier(.image)}) else {
            completion()
            return
        }
        
        performBatchImageFetching(forTypeIdentifier: typeIdentifier, using: attachments, currentImageIndex: 0,request: request, partialFetchHandler: { (imageData, index) in
            partialFetchHandler(imageData, index)
        }) {
            completion()
        }
        
    }
    
    
    
    //MARK:-  BatchImageFetching USING Data List
    private func performBatchImageFetching(forTypeIdentifier typeIdentifier: UTI,using datas: [NSItemProvider],
                                           currentImageIndex: Int,
                                           request:NSItemProvider.RequestType,
                                           partialFetchHandler: @escaping (_ imageData: AFSImage?, _ index: Int) -> Void,
                                           completion: @escaping () -> Void) {
        
        guard currentImageIndex < datas.count else {
            completion()
            return
        }
        datas[currentImageIndex].fetchImageData(forTypeIdentifier: typeIdentifier,request: request) { imageData in
            partialFetchHandler(imageData, currentImageIndex)
            self.performBatchImageFetching(forTypeIdentifier :typeIdentifier,using: datas, currentImageIndex: currentImageIndex + 1,request: request, partialFetchHandler: partialFetchHandler){
                completion()
            }
        }
        
    }
    
    
}
extension NSItemProvider{
    enum DataType {
        case image(UIImage)
        case url(URL)
        case data(Data)
    }
    enum RequestType:Int {
        case item
        case previewImage
        case dataRepresent
    }
    func hasItemConformingToTypeIdentifier(_ typeIdentifier: UTI) -> Bool{
        return self.hasItemConformingToTypeIdentifier(typeIdentifier.rawValue)
    }
    func loadPreviewImage(options: [AnyHashable : Any]! = [:], completionHandler:@escaping (Result<DataType,Error>)->Void){
        self.loadPreviewImage(options: options) { (item, error) in
            if let error = error{
                completionHandler(.failure(error))
            }else if let vl  = item{
                switch vl {
                case let image as UIImage:
                    completionHandler(.success(.image(image)))
                case let data as Data:
                    completionHandler(.success(.data(data)))
                case let url as URL:
                    completionHandler(.success(.url(url)))
                default:
                    break
                }
                
            }
        }
    }
    
    func hasRepresentationConforming(toTypeIdentifier typeIdentifier: UTI, fileOptions: NSItemProviderFileOptions = []) -> Bool{
        return self.hasRepresentationConforming(toTypeIdentifier: typeIdentifier.rawValue, fileOptions: fileOptions)
    }
    func loadDataRepresentation(forTypeIdentifier typeIdentifier: UTI, completionHandler: @escaping (Result<Data,Error>) -> Void){
         self.loadDataRepresentation(forTypeIdentifier: typeIdentifier.rawValue) {(data , error) in
            if let err = error{
                completionHandler(.failure(err))
            }else if let data  = data{
                
                completionHandler(.success(data))
            }
        }
    }
    func loadItem(forTypeIdentifier typeIdentifier: UTI, options: [AnyHashable : Any]? = nil, completionHandler: ((Result<DataType,Error>)->Void)? = nil){
        
        if self.hasItemConformingToTypeIdentifier(typeIdentifier) {
            self.loadItem(forTypeIdentifier: typeIdentifier.rawValue, options: options) { (item, error) in
                if let error = error{
                    completionHandler?(.failure(error))
                }else if let vl = item{
                    switch vl {
                    case let image as UIImage:
                        completionHandler?(.success(.image(image)))
                    case let data as Data:
                        completionHandler?(.success(.data(data)))
                    case let url as URL:
                        completionHandler?(.success(.url(url)))
                    default:
                        break
                    }
                }
            }
        }else{
            print("\(typeIdentifier.rawValue) not available")
            completionHandler?(.failure(AFSError.noContentType(typeIdentifier)))
        }
        
    }
    
    
    
    func fetchImageData(forTypeIdentifier typeIdentifier: UTI,request:RequestType,completion: @escaping (_ imageData: AFSImage?) -> Void){
        DispatchQueue.global(qos: .background).async {
            let options =  [NSItemProviderPreferredImageSizeKey: NSValue(cgSize: CGSize(width: 1024, height: 1024))]
            if request == .item{
                self.loadItem(forTypeIdentifier: typeIdentifier, options: options) { (result) in
                    switch result{
                    case .success(let dataType):
                        switch dataType {
                        case .data(let imageData):
                            let rs = FileManager.write(dataType: imageData)
                            if rs.isWrite{
                                if let image = FileManager.getImage(filename: rs.fileName){
                                    completion(image)
                                }
                            }
                        case .image(let image):
                           let rs = FileManager.write(dataType: image)
                            if rs.isWrite{
                                if let image = FileManager.getImage(filename: rs.fileName){
                                    completion(image)
                                }
                            }
                            
                        case .url(let url):
                            let rs = FileManager.write(dataType: url)
                            if rs.isWrite{
                                if let image = FileManager.getImage(filename: rs.fileName){
                                    completion(image)
                                }
                            }
                            
                        }
                    case .failure(let error):
                        print("\(typeIdentifier.rawValue) not available Error:\n \(error.localizedDescription)")
                        //context.cancelRequest(withError: error)
                        completion(nil)
                    }
                }
            }else if request == .previewImage{
                if self.hasRepresentationConforming(toTypeIdentifier: typeIdentifier){
                    
                    self.loadPreviewImage(options: options) { (result) in
                        switch result{
                        case .success(let dataType):
                            switch dataType {
                            case .data(let imageData):
                                let rs = FileManager.write(dataType: imageData)
                                if rs.isWrite{
                                    if let image = FileManager.getImage(filename: rs.fileName){
                                        completion(image)
                                    }
                                }
                            case .image(let image):
                                
                                self.loadDataRepresentation(forTypeIdentifier: typeIdentifier) { (result) in
                                    switch result{
                                    case .failure(let error):
                                        print(error)
                                        completion(nil)
                                    case .success(let imageData):
                                        completion(AFSImage(image: image,data:imageData))
                                        
                                    }
                                }
                                
                                /*  let rs = FileManager.write(dataType: image)
                                 if rs.isWrite{
                                 if let image = FileManager.getImage(filename: rs.fileName){
                                 completion(image)
                                 }
                                 }*/
                                
                            case .url(let url):
                                let rs = FileManager.write(dataType: url)
                                if rs.isWrite{
                                    if let image = FileManager.getImage(filename: rs.fileName){
                                        
                                        completion(image)
                                    }
                                }
                                
                            }
                        case .failure(let error):
                            print("\(typeIdentifier.rawValue) not available Error:\n \(error.localizedDescription)")
                            //context.cancelRequest(withError: error)
                            completion(nil)
                        }
                    }
                    
                }
                
            }else{
                if self.hasRepresentationConforming(toTypeIdentifier: typeIdentifier){
                    _ =  self.loadDataRepresentation(forTypeIdentifier: typeIdentifier) { (result) in
                        switch result{
                        case .failure(let error):
                            print(error)
                            completion(nil)
                        case .success(let imageData):
                            if let fileName = FileManager.imageFileName(dataType: imageData), let image = FileManager.getImage(filename: fileName) {
                                completion(image)
                            }else {
                                let res = FileManager.write(dataType: imageData)
                                if let image = FileManager.getImage(filename: res.fileName){
                                    
                                    completion(image)
                                }
                                
                            }
                            
                        }
                    }
                }else{
                    print("\(typeIdentifier.rawValue) not available")
                    completion(nil)
                }
            }
            
        }
        
    }
    
}
enum AFSError:Error {
    case noAttachment
    case cancelled
    case noContentType(UTI)
    var localizedDescription: String{
        switch self {
        case .noAttachment: return "AFS Share attachment is not avialable"
        case .cancelled:return "User Cancelled the request."
        case .noContentType(let uti):
            return "AFS Share \(uti.rawValue) attachment is not avialable"
            
            
        }
    }
    
}

extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
    
    
}

