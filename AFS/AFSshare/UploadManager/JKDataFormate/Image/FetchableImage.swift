//
//  FetchableImage.swift
//  FetchableImageDemo
//
//  Created by Gabriel Theodoropoulos.
//  Copyright © 2020 AppCoda. All rights reserved.
//

import Foundation
import UIKit
import CoreImage

protocol FetchableImage {
    
    
    func localFileURL<T>(for dataType: T?, options: FetchableImageOptions?) -> URL?
    func fetchImage<T>(from dataType: T?, options: FetchableImageOptions?, completion: @escaping (_ imageData: Data?) -> Void)
    
    
    func fetchBatchImages<T>(using dataTypes: [T?],
                             options: FetchableImageOptions?,
                             partialFetchHandler: @escaping (_ imageData: Data?, _ index: Int) -> Void,
                             completion: @escaping () -> Void)
    
    
    
    func deleteImage(using imageURL: String?, options: FetchableImageOptions?) -> Bool
    func deleteBatchImages(using imageURLs: [String?], options: FetchableImageOptions?)
    func deleteBatchImages(using multipleOptions: [FetchableImageOptions])
    func save(image data: Data, options: FetchableImageOptions) -> Bool
    
    
    
    
    
    func deleteDirectory()
    
    
    
}


extension FetchableImage {
    
    func localFileURL<T>(for dataType: T?, options: FetchableImageOptions? = nil) -> URL? {
        
        let opt = FetchableImageHelper.getOptions(options)
        guard let targetDir = opt.storeInCachesDirectory ?
            FetchableImageHelper.cachesDirectoryURL :
            FetchableImageHelper.documentsDirectoryURL else{return nil}
        if let imageData = dataType as? Data{
            if let customFileName = opt.customFileName  {
                return targetDir.appendingPathComponent(customFileName)
                
            }else if let imageName = FetchableImageHelper.getImageName(from: imageData){
                return targetDir.appendingPathComponent(imageName)
            }else{
                return nil
            }
            
            
            
        }else if let  urlString = dataType as? String{
            if let customFileName = opt.customFileName  {
                return targetDir.appendingPathComponent(customFileName)
                
            }else if let imageName = FetchableImageHelper.getImageName(from: urlString){
                return targetDir.appendingPathComponent(imageName)
            }else{
                return nil
            }
        }else if let  url = dataType as? URL{
            if let customFileName = opt.customFileName  {
                return targetDir.appendingPathComponent(customFileName)
            }else{
                return targetDir.appendingPathComponent(url.lastPathComponent)
            }
        }else{
            return nil
        }
        
    }
    
    
    
    func fetchImage<T>(from dataType: T?, options: FetchableImageOptions? = nil, completion: @escaping (_ imageData: Data?) -> Void) {
        DispatchQueue.global(qos: .background).async {
            let opt = FetchableImageHelper.getOptions(options)
            let localURL = self.localFileURL(for: dataType, options: options)
            
            // Determine if image exists locally first.
            if opt.allowLocalStorage,
                let localURL = localURL,
                FileManager.default.fileExists(atPath: localURL.path) {
                
                // Image exists locally!
                // Load it using the composed localURL.
                let loadedImageData = FetchableImageHelper.loadLocalImage(from: localURL)
                completion(loadedImageData)
                
            } else {
                // Image does not exist locally!
                // Download it.
                guard let dataType = dataType else {
                    completion(nil)
                    return
                    
                }
                
                if let url = dataType as? URL {
                    if url.path.lowercased().hasPrefix("https") || url.path.lowercased().hasPrefix("http"){
                        FetchableImageHelper.downloadImage(from: url) { (imageData) in
                            if opt.allowLocalStorage, let localURL = localURL {
                                try? imageData?.write(to: localURL)
                            }
                            
                            completion(imageData)
                        }
                    }else{
                        guard let imageData = FetchableImageHelper.loadLocalImage(from: url) else {
                            completion(nil)
                            return
                        }
                        do {
                            if opt.allowLocalStorage, let localURL = localURL {
                                try imageData.write(to: localURL)
                                completion(imageData)
                            }
                            
                        } catch  {
                            completion(nil)
                        }
                    }
                }else if let urlString  = dataType as? String{
                    if let url = URL(string: urlString) {
                        FetchableImageHelper.downloadImage(from: url) { (imageData) in
                            if opt.allowLocalStorage, let localURL = localURL {
                                try? imageData?.write(to: localURL)
                            }
                            
                            completion(imageData)
                        }
                    }else{
                        
                        guard let imageData = FetchableImageHelper.loadLocalImage(from: URL(fileURLWithPath: urlString)) else {
                            completion(nil)
                            return
                        }
                        do {
                            if opt.allowLocalStorage, let localURL = localURL {
                                try imageData.write(to: localURL)
                                completion(imageData)
                            }
                            
                        } catch  {
                            completion(nil)
                        }
                        
                    }
                }
                
                
                
                
                
            }
        }
    }
    
    
    
    func fetchBatchImages<T>(using dataTypes: [T?],
                          options: FetchableImageOptions? = nil,
                          partialFetchHandler: @escaping (_ imageData: Data?, _ index: Int) -> Void,
                          completion: @escaping () -> Void) {
        
        performBatchImageFetching(using: dataTypes, currentImageIndex: 0, options: options, partialFetchHandler: { (imageData, index) in
            partialFetchHandler(imageData, index)
        }) {
            completion()
        }
        
    }
    
    //MARK:-  BatchImageFetching USING Image URL String List
    private func performBatchImageFetching<T>(using dataTypes: [T?],
                                           currentImageIndex: Int,
                                           options: FetchableImageOptions?,
                                           partialFetchHandler: @escaping (_ imageData: Data?, _ index: Int) -> Void,
                                           completion: @escaping () -> Void) {
        
        guard currentImageIndex < dataTypes.count else {
            completion()
            return
        }
        
        
        fetchImage(from: dataTypes[currentImageIndex], options: options) { (imageData) in
            partialFetchHandler(imageData, currentImageIndex)
            
            self.performBatchImageFetching(using: dataTypes, currentImageIndex: currentImageIndex + 1, options: options, partialFetchHandler: partialFetchHandler) {
                
                completion()
            }
        }
    }
    
 
    
    
    func deleteImage(using imageURL: String?, options: FetchableImageOptions? = nil) -> Bool {
        guard let localURL = localFileURL(for: imageURL, options: options),
            FileManager.default.fileExists(atPath: localURL.path) else { return false }
        
        do {
            try FileManager.default.removeItem(at: localURL)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    
    
    func deleteBatchImages(using imageURLs: [String?], options: FetchableImageOptions? = nil) {
        DispatchQueue.global().async {
            imageURLs.forEach { _ = self.deleteImage(using: $0, options: options) }
        }
    }
    
    
    func deleteBatchImages(using multipleOptions: [FetchableImageOptions]) {
        DispatchQueue.global().async {
            multipleOptions.forEach { _ = self.deleteImage(using: nil, options: $0) }
        }
    }
    
    
    func save(image data: Data, options: FetchableImageOptions) -> Bool {
        guard let url = localFileURL(for: data, options: options) else { return false }
        do {
            try data.write(to: url)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    func deleteDirectory(){
        FetchableImageHelper.deleteDirectory()
    }
    
}


struct FetchableImageOptions {
    var storeInCachesDirectory: Bool = true
    var allowLocalStorage: Bool = true
    var customFileName: String?
}


struct FetchableImageHelper {
    static var documentsDirectoryURL = FileManager.documentsDirectoryURL//FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(Bundle.kAppGroupId)
    static var cachesDirectoryURL = FileManager.cachesDirectoryURL//FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0].appendingPathComponent(Bundle.kAppGroupId)
    
    static func deleteDirectory(){
        
        if let documentsDirectoryURL = documentsDirectoryURL, FileManager.default.fileExists(atPath: documentsDirectoryURL.path){
            do {
                try FileManager.default.removeItem(at: documentsDirectoryURL)
                
            } catch  {
                print(error.localizedDescription)
            }
        }
        if let cachesDirectoryURL = cachesDirectoryURL, FileManager.default.fileExists(atPath: cachesDirectoryURL.path){
            do {
                try FileManager.default.removeItem(at: cachesDirectoryURL)
                
            } catch  {
                print(error.localizedDescription)
            }
        }
    }
    static func getOptions(_ options: FetchableImageOptions?) -> FetchableImageOptions {
        return options != nil ? options! : FetchableImageOptions()
    }
    
    
    static func getImageName<T>(from dataType: T?) -> String? {
        if let urlString = dataType as? String {
            if urlString.lowercased().hasPrefix("https") || urlString.lowercased().hasPrefix("http") {
                return URL(string: urlString)?.lastPathComponent
            }else{
                return URL(fileURLWithPath: urlString).lastPathComponent
            }
        }else if let imageData  = dataType as? Data{
            return imageData.fileName
        }else if let image  = dataType as? UIImage{
            return image.fileName
        }else{
            return nil
        }
        
    }
    
    static func downloadImage(from url: URL, completion: @escaping (_ imageData: Data?) -> Void) {
        let sessionConfiguration = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: sessionConfiguration)
        let task = session.dataTask(with: url) { (data, response, error) in
            completion(data)
        }
        task.resume()
    }
    
    
    
    
    static func loadLocalImage(from url: URL) -> Data? {
        do {
            let imageData = try Data(contentsOf: url)
            return imageData
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}
