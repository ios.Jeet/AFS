//
//  JKDataFormate.swift
//  Pivot
//
//  Created by Jitendra Kumar on 08/04/20.
//  Copyright © 2020 Jitendra Kumar. All rights reserved.
//

import UIKit
import MobileCoreServices

extension UIImage{
    subscript (imageFormate:ImageFormate)->Data?{
        return imageFormate.imageData(image: self)
    }
    var data:Data?{
        if let d = self[.png] {
            return d
        }else if let d = self[.jpeg(quality: .highest)]{
            return d
        }else if let d = self[.jpeg(quality: .high)]{
            return d
        }else if let d = self[.jpeg(quality: .medium)]{
            return d
        }else{
            return nil
        }
    }
    
}
public enum JPEGQuality:Int,CustomStringConvertible {
    case highest
    case high
    case medium
    case low
    case lowest
    subscript(image:UIImage)->Data?{
        switch self {
        case .highest:return image.jpegData(compressionQuality: 1.0)
        case .high: return image.jpegData(compressionQuality: 0.75)
        case .medium: return image.jpegData(compressionQuality: 0.5)
        case .low:  return image.jpegData(compressionQuality: 0.25)
        case .lowest:  return image.jpegData(compressionQuality: 0.1)
            
        }
    }
    public var description: String{
        switch self {
        case .highest :return "highest Quality JPEG Formate data"
        case .high    :return "high Quality JPEG Formate data"
        case .medium  :return "medium Quality JPEG Formate data"
        case .low     :return "low Quality JPEG Formate data"
        case .lowest  :return "lowest Quality JPEG Formate data"
        }
    }
}
public enum ImageFormate:CustomStringConvertible{
    
    case png
    case jpeg(quality:JPEGQuality)
    public func imageData(image:UIImage)->Data?{
        switch self {
        case .jpeg(let quality):
            return quality[image]
        case .png:
            return image.pngData()
            
        }
    }
    public var fileExtension: String{
        switch self {
        case .png:
            return "png"
        default:
            return "jpeg"
        }
    }
    public var mimeType:String{
        switch self {
        case .png:
            return "image/png"
        default:
            return "image/jpeg"
        }
    }
    public var description: String{
        switch self {
        case .png:
            return "Image Quality PNG Formate data"
        case .jpeg(let quality):return quality.description
            
        }
        
    }
    
    
}


public struct MultiPartData {
    var data:Data?
    var fileExtension:String = ""
    var fileName:String = ""
    var mimeType:String = ""
    var uploadKey:String = ""
    
    public init(imageData:Data,fileName:String? = nil,uploadKey:String = "",email:String) {
        
        self.data           = imageData
        self.fileExtension  = imageData.fileExtension
        self.fileName       = fileName ?? "IMG_\(email)_\(Date().timeIntervalSince1970).\(imageData.fileExtension)"
        self.mimeType       = imageData.mimeType
        self.uploadKey      = uploadKey
    }
    public init(image:UIImage,fileName:String? = nil,uploadKey:String = "",formate:ImageFormate = .png,email:String) {
        
        self.data          = formate.imageData(image: image)
        self.fileExtension  = formate.fileExtension
        self.fileName       = fileName ?? "IMG_\(email)_\(Date().timeIntervalSince1970).\(formate.fileExtension)"
        self.mimeType        = formate.mimeType
        self.uploadKey = uploadKey
    }
    
    public init(file:Any,mediaKey uploadKey:String = "") {
        self.uploadKey = uploadKey
        if let filepath = file as? String{
            let url = NSURL.fileURL(withPath: filepath)
            self.fileExtension = url.pathExtension
            self.fileName = url.lastPathComponent
            self.mimeType  = url.mimeType
            do{
                self.data = try Data(contentsOf: url)
            }catch{
                print("Unable Convert Data")
            }
            
        }else if let url = file as? URL {
            self.fileExtension = url.pathExtension
            self.fileName = url.lastPathComponent
            self.mimeType  = url.mimeType
            do{
                self.data = try Data(contentsOf: url)
                
            }catch{
                print("Unable Convert Data")
            }
            
        }
        
        
    }
}

public extension URL{
    
    var mimeType:String{
        let pathExtension = self.pathExtension
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
}


