//
//  JKTextField.swift
// JKMaterialKit
//
//  Created by Jitendra Kumar on 27/12/16.
//  Copyright © 2016 Jitendra. All rights reserved
//

import UIKit
import QuartzCore

@IBDesignable

open class JKTextField : UITextField {
    
    typealias AccessoryIconClickEventBlock = (_ sender:UIButton)->Void
    var clickEventBlock:AccessoryIconClickEventBlock!
    @IBInspectable public var padding: CGSize = CGSize(width: 5, height: 5)
    @IBInspectable public var floatingLabelBottomMargin: CGFloat = 2.0
    @IBInspectable public var enabledFloatingPlaceholder: Bool = false
    @IBInspectable public var floatingLabelFont: UIFont = UIFont.systemFont(ofSize: 14)
    @IBInspectable public var paddingButton: UIButton?
    
    
   
    @IBInspectable public var leftIcon: UIImage? {
        didSet {
            if leftIcon == nil {
                self.leftView = nil
                self.leftViewMode = .never
                
            }else{
                leftBtn.setImage(leftIcon, for: UIControl.State())
                self.left()
                
            }
            self.setNeedsDisplay()
        }
    }
    
  
    
    @IBInspectable public var rightIcon: UIImage? {
        didSet {
            if rightIcon == nil {
                self.rightView = nil
                self.rightViewMode = .never
                
            }else{
                rightBtn.setImage(rightIcon, for: UIControl.State())
                self.right()
                
            }
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable public var shadowColor: UIColor = UIColor.black {
        didSet {
            layer.shadowColor = shadowColor.cgColor
        }
    }
    @IBInspectable public var shadowOpacity: Float = 0.5 {
        didSet {
            layer.shadowOpacity = shadowOpacity
        }
    }
    @IBInspectable public var shadowOffset: CGSize = CGSize(width: 0, height: 3) {
        didSet {
            layer.shadowOffset = shadowOffset
        }
    }
    @IBInspectable public var shadowRadius : CGFloat = 3{
        didSet{
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable public var cornerRadius: CGFloat = 2.5 {
        didSet {
            layer.cornerRadius = cornerRadius
            
        }
    }
    @IBInspectable public var boarderColor: UIColor = UIColor.clear{
        didSet{
            layer.borderColor = boarderColor.cgColor
        }
    }
    @IBInspectable public var borderWidth: CGFloat =  0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable public var masksToBounds : Bool = false{
        didSet{
            layer.masksToBounds = masksToBounds
        }
    }
    @IBInspectable public var clipsToBound : Bool = false{
        didSet{
            self.clipsToBounds = clipsToBound
        }
    }
    @IBInspectable public var placeholderColor : UIColor = .gray{
        didSet{
            self.attibutePlaceholder(color: placeholderColor)
        }
    }
    @IBInspectable public var clearButtonColor:UIColor = .white{
        didSet{
            self.setClearButtonColor()
        }
    }
    
    // floating label
    @IBInspectable public var floatingTextColor: UIColor = .lightGray {
        didSet {
            floatingLabel.textColor = floatingTextColor
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable public var enabledBottomBar: Bool = false {
        didSet {
            bottomBarLayer?.removeFromSuperlayer()
            bottomBarLayer = nil
            if enabledBottomBar {
                bottomBarLayer = CALayer()
                bottomBarLayer?.frame = CGRect(x: 0, y: layer.bounds.height - 1, width: bounds.width, height: 1)
                bottomBarLayer?.backgroundColor = UIColor.gray.cgColor
                layer.addSublayer(bottomBarLayer!)
                
            }
            self.setNeedsDisplay()
        }
    }
    @IBInspectable public var bottomBarWidth: CGFloat = 1.0
    @IBInspectable public var bottomBarColor: UIColor = UIColor.lightGray{
        didSet{
            self.setNeedsDisplay()
        }
    }
    @IBInspectable public var bottomBarHighlightWidth: CGFloat = 1.75{
        didSet{
            self.setNeedsDisplay()
        }
    }
    
    override open var placeholder: String? {
        didSet {
            updateFloatingLabelText()
            self.setNeedsDisplay()
        }
    }
    fileprivate var floatingLabel: UILabel!
    private var bottomBarLayer: CALayer?
    
    //MARK:- setClearButtonColor
    public func setClearButtonColor(){
        if self.clearButtonMode != .never  {
            if let clearButton = self.value(forKeyPath: "_clearButton") as? UIButton {
                if let img = UIImage(named:"clearButton") {
                    clearButton.setImage(img.withRenderingMode(.alwaysTemplate), for: .normal)
                    clearButton.tintColor = clearButtonColor
                }
            }
            self.setNeedsLayout()
        }
        
    }
    
    func attibutePlaceholder(color:UIColor){
        if let placeholder = self.placeholder{
            self.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: color])
        }else{
            self.attributedPlaceholder = nil
        }
        self.setNeedsDisplay()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupLayer()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayer()
    }
    //MARK:- setupLayer
    private func setupLayer() {
        cornerRadius = 2.5
        layer.borderWidth = 1.0
        borderStyle = .none
        // floating label
        floatingLabel = UILabel()
        floatingLabel.font = floatingLabelFont
        floatingLabel.alpha = 0.0
        floatingLabel.textAlignment = textAlignment
        self.updateFloatingLabelText()
        addSubview(floatingLabel)
    }
    
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        if enabledBottomBar {
            bottomBarLayer?.backgroundColor = isFirstResponder ? tintColor.cgColor : bottomBarColor.cgColor
            let borderWidth = isFirstResponder ? bottomBarHighlightWidth : bottomBarWidth
            bottomBarLayer?.frame = CGRect(x: 0, y: layer.bounds.height - borderWidth, width: layer.bounds.width, height: borderWidth)
        }
        if enabledFloatingPlaceholder {
            if !text!.isEmpty {
                floatingLabel.textColor = isFirstResponder ? tintColor : floatingTextColor
                if floatingLabel.alpha == 0 {
                    showFloatingLabel()
                }
            } else {
                hideFloatingLabel()
            }
        }
        setFloatingLabelTextAligment()
        setClearButtonColor()
        
    }
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.textRect(forBounds: bounds)
        var newRect = CGRect(x: rect.origin.x + padding.width, y: rect.origin.y,
                             width: rect.size.width - 2*padding.width, height: rect.size.height)
        if !enabledFloatingPlaceholder {
            return newRect
        }
        
        if !text!.isEmpty {
            var dTop = ceil(floatingLabel.font.lineHeight + floatingLabelBottomMargin)
            dTop = min(dTop, maxTopInset())
            //   let dTop = floatingLabel.font.lineHeight + floatingLabelBottomMargin
            newRect = newRect.inset(by: UIEdgeInsets(top: dTop+5, left: 0.0, bottom: 0.0, right: 0.0))
        }
        return newRect
    }
    fileprivate func maxTopInset()->CGFloat {
        if let fnt = font {
            return max(0, floor(bounds.size.height - fnt.lineHeight - 4.0))
        }
        return 0
    }
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    
    
    //MARK - private methods
    //MARK:- setFloatingLabelTextAligment
    private func setFloatingLabelTextAligment() {
        let textRect = self.textRect(forBounds: bounds)
        var originX = textRect.origin.x
        switch textAlignment {
        case .center:
            originX = textRect.origin.x + (textRect.size.width * 0.5) - floatingLabel.frame.size.width
        case .right:
            //originX += textRect.size.width - floatingLabel.bounds.width
            originX = textRect.origin.x + textRect.size.width - floatingLabel.bounds.width
        default:
            // originX += 30
            if let icon = leftBtn.image(for: UIControl.State())  {
                var iconWidth  = icon.size.width
                let iconHeight  = self.bounds.size.height
                if iconWidth > self.bounds.size.width {
                    iconWidth = iconHeight
                }
                originX += iconWidth
                
            }else{
                originX += padding.width
                
            }
            
            break
        }
        
        floatingLabel.frame = CGRect(x: originX, y: padding.height,
                                     width: floatingLabel.frame.size.width, height: floatingLabel.frame.size.height)
        self.layoutIfNeeded()
    }
    //MARK:- showFloatingLabel
    private func showFloatingLabel() {
        let curFrame = floatingLabel.frame
        floatingLabel.frame = CGRect(x: curFrame.origin.x, y: bounds.height/2, width: curFrame.width, height: curFrame.height)
        UIView.animate(withDuration: 0.45, delay: 0.0, options: .curveEaseOut,
                       animations: {
                        self.floatingLabel.alpha = 1.0
                        self.floatingLabel.frame = curFrame
        }, completion: nil)
    }
    //MARK:- hideFloatingLabel
    private func hideFloatingLabel() {
        floatingLabel.alpha = 0.0
    }
    //MARK:- updateFloatingLabelText
    private func updateFloatingLabelText() {
        floatingLabel.text = placeholder
        floatingLabel.sizeToFit()
        setFloatingLabelTextAligment()
    }
    
    public typealias Action = (JKTextField) -> Void
    fileprivate var actionEditingChanged: Action?
    public func didChangeAction(closure: @escaping Action) {
        if actionEditingChanged == nil {
            addTarget(self, action: #selector(JKTextField.textFieldDidChange), for: .editingChanged)
        }
        actionEditingChanged = closure
    }
    
    @objc func textFieldDidChange(_ textField: JKTextField) {
        actionEditingChanged?(self)
    }
    
    
    
    //MARK: - PADDING VIEW
    
    internal lazy var leftBtn:UIButton = {
        let button = UIButton(type: .custom)
        return button
    }()
    internal lazy var rightBtn:UIButton = {
        let button = UIButton(type: .custom)
        return button
    }()
    //MARK:- leftView -
    func left(onCompletion :AccessoryIconClickEventBlock? = nil){
        guard let icon = leftBtn.image(for: UIControl.State()) else {return}
        var iconWidth  = icon.size.width
        let iconHeight  = self.bounds.size.height
        if iconWidth > self.bounds.size.width {
            iconWidth = iconHeight
        }
        
        leftBtn.frame = CGRect(x: 0, y: 0, width:  iconWidth+padding.width, height: iconHeight)
        leftBtn.imageEdgeInsets = UIEdgeInsets.init(top: 2, left: 2, bottom: 0, right: 0)//tlbr
        if let handler =  onCompletion  {
            clickEventBlock = handler
            leftBtn.addTarget(self, action:#selector(leftBtnClick), for: .touchUpInside)
        }
        
        self.leftView = leftBtn
        self.leftViewMode = .always
        
    }
    @objc fileprivate func leftBtnClick(_ sender:UIButton){
        let simage = sender.image(for: .selected)
        if  (simage != nil) {
            sender.isSelected =   sender.isSelected == false ? true : false
        }
        if (clickEventBlock != nil) {
            clickEventBlock(sender)
        }
    }
    
    //MARK:- rightView -
    func right(onCompletion :AccessoryIconClickEventBlock? = nil){
        guard let icon = rightBtn.image(for: UIControl.State()) else { return}
        var iconWidth  = icon.size.width
        let iconHeight  = self.bounds.size.height
        if iconWidth > self.bounds.size.width {
            iconWidth = iconHeight+padding.width
        }
        rightBtn.frame = CGRect(x: 0, y: 0, width:  iconWidth, height: iconHeight )
        rightBtn.imageEdgeInsets = UIEdgeInsets.init(top: 2, left: 0, bottom: 0,right: 2)
        
        if let handler  = onCompletion {
            clickEventBlock = handler
            rightBtn.addTarget(self, action:#selector(rightBtnClick), for: .touchUpInside)
        }
        self.rightView = rightBtn
        self.rightViewMode = .always
    }
    @objc func rightBtnClick(_ sender:UIButton){
        let simage = sender.image(for: .selected)
        if  (simage != nil) {
            sender.isSelected =  sender.isSelected == false ? true : false
        }
        if (clickEventBlock != nil) {
            clickEventBlock(sender)
        }
    }
    
}



//MARK:- EXTENSION FOR TEXTFIELD
private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return Int.max//150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
    
    
    
}
