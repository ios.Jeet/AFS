//
//  AFSClient.swift
//  AFS
//
//  Created by Jitendra Kumar on 29/06/20.
//  Copyright © 2020 Jitendra Kumar. All rights reserved.
//


import UIKit
import Alamofire


public extension CBServer{
    
    enum MultiPartType {
        case data([Data?])
        case images([UIImage])
        
        
        var count:Int{
            switch self {
            case .data(let list):return list.count
            case .images(let list):return list.count
                
            }
        }
        var imageDataList:[Data]{
            switch self {
            case .data(let list):return list.compactMap({$0})
            default: return []
                
            }
        }
        var imageList:[UIImage]{
            switch self {
                
            case .images(let list):return list
            default: return []
                
            }
        }
        var items:[Any]{
            switch self {
            case .data(let list):return list.compactMap({$0})
            case .images(let list):return list
                
            }
        }
        
    }
    var isConnected:Bool {
        return NetworkReachabilityManager()!.isReachable
        
    }
    func upload(_ state:CBSessionConfig.State = .default,
                _ mulipartType:MultiPartType,
                description:String,
                email:String,
                numOfImages:Int,
                orchard:String,
                block:String,
                row:String,
                UDIDString:String,
                completion:@escaping(Swift.Result<Int, Error>)->Void,uploadProgress:@escaping(Progress)->Void){
        var list:[MultiPartData] = []
        switch mulipartType {
        case .data(let muliparts):
            list =  muliparts.compactMap({$0}).compactMap({MultiPartData(imageData: $0,uploadKey: "file",email: email)})
        case .images(let muliparts):
            list = muliparts.compactMap({MultiPartData(image: $0, uploadKey: "file", formate: .png,email: email)})
        }
        
        self.uploadTask(state, endpoint: .upload(.upload), method: .post,parameters:["description":description,"numOfImages":numOfImages,"Orchard":orchard,"Block":block,"Row":row,"UDID":UDIDString], encoding: .URL,multiPartList: list, completion: { result in
            async {
                switch result{
                case .success(let status):
                    completion(.success(status))
                    
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }, uploadProgress:uploadProgress)
    }
    
    
    
    
}

final class PickerViewModel {
    private init(){}
    static let shared = PickerViewModel()
    var dropdownList:[AFSPickerData] = []
    var blocklist:[String] = []
    func getDropdownData(email:String,completion:@escaping(_ error:String?)->Void){
        guard CBServer.shared.isConnected, !email.isEmpty else {
            return
        }
        let handler  = {  (_ result:Swift.Result<Data,Error>) in
            switch result {
            case .success(let data):
                let res = data.JKDecoder(AFSPickerResponse.self)
                switch res {
                case .success(let vl):
                    self.dropdownList = vl.meta
                    completion(nil)
                case .failure(let error):
                    completion(error.localizedDescription)
                }
            case .failure(let error):
                completion(error.localizedDescription)
            }
        }
        CBServer.shared.dataTask(.default, endpoint: .upload(.picker), method: .get, parameters: ["email":email], encoding: .URL, headers: nil,completion: handler)
        
    }
}
extension PickerViewModel{
    var count:Int{
        return dropdownList.count
    }
    subscript(atOrchart index:Int)->AFSPickerData?{
        guard index<count else{return nil}
        return dropdownList[index]
    }
    
    
    func set(_ blocks:[String]) {
        self.blocklist = blocks
    }
    var blockCount:Int{
        return blocklist.count
    }
    subscript(atBlock index:Int)->String?{
        guard index<blockCount else{return nil}
        return blocklist[index]
    }
    func removeAll(){
        self.dropdownList.removeAll()
        self.blocklist.removeAll()
    }
}
