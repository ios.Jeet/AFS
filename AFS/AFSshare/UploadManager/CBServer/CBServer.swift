//
//  CBServer.swift
//  ChatterBox
//
//  Created by Jitendra Kumar on 22/05/20.
//  Copyright © 2020 Jitendra Kumar. All rights reserved.
//

import UIKit
import Alamofire

public typealias  CBServerResponse<Success> = (Swift.Result<Success, Error>)->Void
public typealias CBServerProgress = (Progress) -> Void
public typealias CBHTTPHeaders = HTTPHeaders
public typealias CBHTTPHeader = HTTPHeaders
public typealias CBParameters = Parameters

open class CBServer: NSObject {
    static public let shared = CBServer()
    public enum CBEncoder {
        
        case URL
        case JSON
        
        var `default`:ParameterEncoding{
            switch self {
            case .JSON:return JSONEncoding.default
            default: return URLEncoding.default
                
            }
        }
    }
    
    
    //MARK:- dataTask
    open func dataTask(_ state:CBSessionConfig.State = .default, endpoint: CBEndpoint,
                       method: HTTPMethod = .post,
                       parameters: CBParameters? = nil,
                       encoding: CBEncoder = .JSON,
                       headers: CBHTTPHeaders? = nil,completion:@escaping CBServerResponse<Data>){
        print(String(describing: parameters))
        CBSession.shared.request(state,url: endpoint.url, method: method, parameters: parameters, encoding: encoding.default, headers: headers).responseData { dataResponse in
            let result  = dataResponse.result
            switch result{
            case .success(let data):
                completion(.success(data))
            case .failure(let error):
                completion(.failure(error))
            }
            
            
        }.resume()
    }
    
    
    
    
    //MARK:- UploadTask
    open func uploadTask(_ state:CBSessionConfig.State = .default,  endpoint: CBEndpoint,
                         method: HTTPMethod = .post,
                         parameters: CBParameters? = nil,
                         encoding: CBEncoder = .JSON,
                         headers: CBHTTPHeaders? = nil, multiPartList:[MultiPartData]?,completion:@escaping CBServerResponse<Int>,uploadProgress:@escaping CBServerProgress){
        
        
        guard let list  = multiPartList, list.count>0 else {
            completion(.failure(AFError.responseValidationFailed(reason: .dataFileNil)))
            return
        }
        CBSession.shared.upload(state, url: endpoint.url, method: method, headers: headers, multipartFormData: {formData in
            list.forEach { (item) in
                if let data = item.data{
                    formData.append(data, withName: item.uploadKey, fileName: item.fileName, mimeType: item.mimeType)
                }
                
            }
            if let parameters = parameters{
                parameters.forEach { (key,value) in
                    formData.append("\(value)".data(using:.utf8)!, withName: key)
                }
            }
        }, encodingCompletion: {result in
            switch result{
            case .success(let upload, _, _):
                upload.validate(statusCode: 200..<300)
                upload.uploadProgress(closure: uploadProgress)
                upload.responseData { (response) in
                    switch response.result{
                    case .success(let data):
                        print("statusCode:\(String(describing: response.response?.statusCode))")
                        print(String(describing: data.utf8String))
                        if let code  = response.response?.statusCode{
                             completion(.success(code))
                        }
                       
                    case .failure(let err):
                        completion(.failure(err))
                    }
                }
                upload.resume()
                
            case .failure(let error):
                completion(.failure(error))
            }
        })
        
        
    }
    
    
}


extension CBServer{
    
    open func application(_ application: UIApplication,
                          handleEventsForBackgroundURLSession identifier: String,
                          completionHandler: @escaping () -> Void) {
        let bundleIdentifier = CBSessionConfig.bundleIdentifier
        
        //application.backgroundRefreshStatus == .available , identifier == bundleIdentifier
        if identifier == bundleIdentifier{
            
            CBSessionConfig.State.background.session.backgroundCompletionHandler = completionHandler
        }else{
            //alertMessage = "Please enable the background mode before refreshing the data in background."
        }
        
    }
    
}

