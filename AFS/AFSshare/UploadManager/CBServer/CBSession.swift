//
//  CBSession.swift
//  ChatterBox
//
//  Created by Jitendra Kumar on 22/05/20.
//  Copyright © 2020 Jitendra Kumar. All rights reserved.
//

import UIKit
import Alamofire


open class CBSession {
    static let shared = CBSession()
   
    private init() {
       
    }
    //MARK- DataRequest
   open func request(_ state:CBSessionConfig.State = .default, url: URLConvertible,
                 method: HTTPMethod = .get,
                 parameters: Parameters? = nil,
                 encoding: ParameterEncoding = URLEncoding.default,
                 headers: HTTPHeaders? = nil) -> DataRequest{
       
        return state.session.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
    }
    
    
    
    //MARK:- Upload Request
    
   open func upload(_ state:CBSessionConfig.State = .default,
                url: URLConvertible,
                method: HTTPMethod = .post,
                headers: HTTPHeaders? = nil,multipartFormData: @escaping (MultipartFormData) -> Void,encodingCompletion: @escaping(SessionManager.MultipartFormDataEncodingResult)->Void){
        state.session.upload(multipartFormData: multipartFormData, to: url, method: method, headers: headers, encodingCompletion: encodingCompletion)
       
        
    }
}




