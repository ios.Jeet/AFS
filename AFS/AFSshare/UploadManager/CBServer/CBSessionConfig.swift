//
//  CBSessionConfig.swift
//  ChatterBox
//
//  Created by Jitendra Kumar on 29/05/20.
//  Copyright © 2020 Jitendra Kumar. All rights reserved.
//

import Foundation
import Alamofire

open class CBSessionConfig:NSObject{
    public enum State:Int{
        case `default` = 0
        case background
        public var session:Alamofire.SessionManager{
            switch self {
            case .background:return CBSessionConfig.shared.background
            case .default: return CBSessionConfig.shared.default
            }
        }
        public func cancelRequest(_ url:String){
            session.request(url).cancel()
        }
        
        
        
    }
    class open var localNotificationID:String{
        return "UILocalNotification"
    }
    class open var bundleIdentifier:String{
        return (Bundle.main.bundleIdentifier ?? Bundle.kAppGroupId)+".backgroundtransfer"
        
    }
    class open var sharedContainerIdentifier:String{
        return "group.com.Infostride.AFS.AFSshare"
    }
    class public var timeoutIntervalForRequest:TimeInterval{
        return 60
    }
    
    override private init() {
        super.init()
    }
    static let shared = CBSessionConfig()
    fileprivate lazy var background: Alamofire.SessionManager = {
        let backgroundConfig = URLSessionConfiguration.background(withIdentifier:CBSessionConfig.bundleIdentifier)
        // Defines how long a task should wait for new date to arrive. iOS default is 60 seconds. Set to 10 minutes.
        backgroundConfig.timeoutIntervalForRequest = 60 * 10
        // Defines how long to complete an entires task in a session. iOS default is 7 days. Set to 1 day.
        backgroundConfig.timeoutIntervalForResource = 60 * 60 * 24
        backgroundConfig.sharedContainerIdentifier = CBSessionConfig.sharedContainerIdentifier
        backgroundConfig.sessionSendsLaunchEvents = true
        backgroundConfig.waitsForConnectivity = true
        backgroundConfig.allowsCellularAccess = true
        backgroundConfig.networkServiceType = .background
        backgroundConfig.isDiscretionary = true
        
        let session = Alamofire.SessionManager(configuration: backgroundConfig)
        session.startRequestsImmediately = true
        return session
        
        
    }()
    
    fileprivate lazy var `default`: Alamofire.SessionManager = {
        let defaultConfig = URLSessionConfiguration.default
        defaultConfig.timeoutIntervalForRequest = CBSessionConfig.timeoutIntervalForRequest
        defaultConfig.sharedContainerIdentifier = CBSessionConfig.sharedContainerIdentifier
        let session = Alamofire.SessionManager(configuration: defaultConfig)
        return session
    }()
    
    
    
}
