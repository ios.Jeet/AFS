//
//  CBEndpoint.swift
//  ChatterBox
//
//  Created by Jitendra Kumar on 01/06/20.
//  Copyright © 2020 Jitendra Kumar. All rights reserved.
//

import Foundation
import Alamofire

private let kServerType = ServerType.live


public enum CBEndpoint {
    case upload(Upload)
    case custom(String)
    
   public var url:String{
        switch self {
        case .upload(let upload): return upload.url
        case .custom(let val):return "\(val)"
        }
    }
    
   public enum Upload:String {
        case upload = "upload"
        case picker = "getmeta"
        public var url:String {
            switch self {
            case .upload:
                return "\(kServerType.path)file/\(self.rawValue)/"
                
            case .picker:
                 return "\(kServerType.path)\(self.rawValue)/"
            }
        }
        
    }
    
}
enum ServerType:String{
    //http://35.212.199.139:8000/file/upload/
    case dev = "http://35.212.199.139:8000"
    case live = "http://upload.fruitscout.ai:8000"
    var path:String{
        return "\(self.rawValue)/"
    }
    
}
public extension Dictionary {
    var queryString: String {
        var output: String = ""
        for (key,value) in self {
            output +=  "\(key)=\(value)&"
        }
        output = String(output.dropLast())
        return output
    }
    mutating  func merge(with dictionary: Dictionary) {
        dictionary.forEach { updateValue($1, forKey: $0) }
    }
    func merged(with dictionary: Dictionary) -> Dictionary {
        var dict = self
        dict.merge(with: dictionary)
        return dict
    }
}
