//
//  Bundle.swift
//  ADGAP
//
//  Created by Jitendra Kumar on 22/05/20.
//  Copyright © 2020 Jitendra Kumar. All rights reserved.
//
import Foundation
import UIKit

extension Bundle{
    
    private static let bundle = Bundle.main
    static var kAppStoreReceiptURL:URL?{
        return bundle.appStoreReceiptURL
    }
    
    static var kBundleURLTypes:[ [String: AnyObject] ] {
        guard let urlTypes = bundle.object(forInfoDictionaryKey:"CFBundleURLTypes") as? [ [String: AnyObject] ] else{ return []}
        return urlTypes
    }
    static var kBundleURLSchemes:[String]{
        var urlSchemes = [String]()
        for urlType in kBundleURLTypes{
            if let schemes = urlType["CFBundleURLSchemes"] as? [String] {
                for scheme in schemes {
                    urlSchemes.append(scheme)
                }
            }
        }
        return urlSchemes
        
        
    }
    
    static var kBundleDisplayName:String? { guard let name = bundle.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String else{return nil}
        return name
    }
    static var kBundleName:String? {
        guard let name = bundle.object(forInfoDictionaryKey: String(kCFBundleNameKey)) as? String else{return nil}
        return name
    }
    static var kAppTitle:String {
        if let bndlName = kBundleDisplayName,!bndlName.isEmpty {
            return bndlName
        }else if let bndlName = kBundleName,!bndlName.isEmpty{
            return bndlName
        }else{
            return ""
        }
    }
    static var kAppVersionString: String{
        guard let version = bundle.object(forInfoDictionaryKey: String(kCFBundleVersionKey)) as? String else{return ""}
        return version
    }
    static var kBuildNumber: String{
        guard let buildnumber = bundle.object(forInfoDictionaryKey: "CFBundleVersion") as? String else{return ""}
        return buildnumber
    }
    static var kBundleID:String{return bundle.bundleIdentifier ?? ""}
    static var kAppGroupId:String{
        guard let buildnumber = bundle.object(forInfoDictionaryKey: "AppGroupId") as? String else{return "group.com.app.automatedfruitscouting"}
        return buildnumber
        
    }
    static var kAppIcon:UIImage? {
        get{
            guard let icons = bundle.infoDictionary?["CFBundleIcons"]  as? [String: Any], let primaryIcon = icons["CFBundlePrimaryIcon"] as? [String: Any],let iconFiles = primaryIcon["CFBundleIconFiles"] as? [String] else{return nil}
            if let fistIcon = iconFiles.first {
                return UIImage.init(named: fistIcon)
            }else if let lastIcon = iconFiles.last {
                return UIImage.init(named: lastIcon)
            }else{
                return nil
            }
            
            
            
        }
        
    }
    static func path(forResource name:String?,ofType type:String?)->String?{
        return self.bundle.path(forResource: name, ofType: type)
    }
    static func url(forResource name:String?,extension ex:String?)->URL?{
        return self.bundle.url(forResource: name, withExtension: ex)
    }
}
extension FileManager{
    
    enum FileError:Error {
        case notFound
        case noDirectoryPaths
        case fileAlreadyExist
        
    }
    //MARK: - getfileFromDirectory-
    class var cachesDirectoryURL:URL?{
        
        if let dirPath =  NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first {
            return URL(fileURLWithPath: dirPath).appendingPathComponent(Bundle.kAppGroupId)
        }else{
            return nil
        }
    }
    class var documentsDirectoryURL:URL?{
        if let dirPath =  NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            return URL(fileURLWithPath: dirPath).appendingPathComponent(Bundle.kAppGroupId)
        }else{
            return nil
        }
    }
    class func fileDirectoryURL(_ filename:String)->URL?{
        return  documentsDirectoryURL?.appendingPathComponent(filename)
       
    }
    class func createDirectory(){
        if let dirUrl = documentsDirectoryURL {
            if self.fileExists(atPath: dirUrl.path) {
                print("Already directory created.")
                
            }else{
                do {
                    try self.default.createDirectory(atPath: dirUrl.path, withIntermediateDirectories: true, attributes: nil)
                } catch  {
                    print(error.localizedDescription)
                }
            }
        }
        
        
    }
    
    class func deleteDirectory(){
        if let dirUrl = documentsDirectoryURL {
            if self.fileExists(atPath: dirUrl.path) {
                do {
                    try self.default.removeItem(at: dirUrl)
                } catch  {
                    print("directory not created. yet")
                }
            }else{
                print("directory not created. yet")
            }
        }
        
        
    }
    
    class func fileExists(filename: String)->Bool{
        if let fileDirectoryURL = fileDirectoryURL(filename) {
            if self.fileExists(atPath: fileDirectoryURL.path) {
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
    class func createFileURL(filename: String)->URL?{
        if let fileDirectoryURL = fileDirectoryURL(filename){
            if self.fileExists(atPath: fileDirectoryURL.path) {
                return nil
            }else{
                return fileDirectoryURL
            }
        }else{
            return nil
        }
    }
    class func fileURL(filename: String)->URL?{
        if let fileDirectoryURL = fileDirectoryURL(filename){
            if self.fileExists(atPath: fileDirectoryURL.path) {
                return fileDirectoryURL
            }else{
                return nil
            }
        }else{
            return nil
        }
    }
    
    class func fileExists(atPath path: String)->Bool{
        return self.default.fileExists(atPath: path)
    }
    class func fileExists(atFileURL url: URL)->Bool{
        return self.default.fileExists(atPath: url.path)
    }
    
   
    class func write<T>(dataType:T)-> (isWrite:Bool,fileName:String){
        
        if let  path = dataType as? URL{
            
            do {
                let data = try Data(contentsOf: path)
                if let url  = self.createFileURL(filename: path.lastPathComponent){
                    try data.write(to: url)
                    
                    return (true,url.lastPathComponent)
                }else{
                    return (false,"")
                }
            } catch  {
                print(FileError.noDirectoryPaths.localizedDescription)
                return (false,"")
            }
            
        }else if let imageData = dataType as? Data{
            let fileName  = imageData.fileName
            if let url  = self.createFileURL(filename:  fileName){
                
                do{
                    try imageData.write(to: url, options: .atomicWrite)
                    
                    return (true,url.lastPathComponent)
                }catch  {
                    print(FileError.noDirectoryPaths.localizedDescription)
                    return (false,"")
                }
            }else{
                print(FileError.fileAlreadyExist.localizedDescription)
                return (false,"")
            }
            
            
            
        }else if let image = dataType as? UIImage, let imageData = image.data{
            let imageName = image.fileName
            
            if let url  = self.createFileURL(filename: imageName){
                do {
                    try imageData.write(to: url)
                    return (true,url.lastPathComponent)
                }catch  {
                    print(FileError.noDirectoryPaths.localizedDescription)
                    return (false,"")
                }
            }else{
                print(FileError.fileAlreadyExist.localizedDescription)
                return (false,"")
            }
            
        }else{
            return (false,"")
        }
        
    }
    
    
    
    
    
    
    class func getImages()->[AFSImage]{
        
        guard let documentDirectory = documentsDirectoryURL,let directoryContents = try? self.default.contentsOfDirectory(at: documentDirectory, includingPropertiesForKeys: nil) else { return [] }
        var images:[AFSImage] = []
        for imageURL in directoryContents  {
            if let image = UIImage(contentsOfFile: imageURL.path) {
                images.append(.init(image: image, fileDirectioryURL: imageURL))
                // now do something with your image
            } else {
                print("Can't create image from file \(imageURL)")
            }
        }
        return images
        
    }
 
    class func getImage(filename:String)->AFSImage?{
        if let url  = self.fileURL(filename: filename) {
            if let image  = UIImage(contentsOfFile: url.path) {
                let asimage  = AFSImage(image: image,fileDirectioryURL:url)
                return asimage
                
            }else{
                return nil
            }
        }else{
            return nil
        }
        
        
    }
    
    
    
    class func getImages(filenames:[String])->[AFSImage]{
        let samphone  = DispatchSemaphore(value: 1)
        var images:[AFSImage] = []
        filenames.forEach {name in
            if let r = self.getImage(filename: name) {
                images.append(r)
            }
            
        }
        
        if images.count == filenames.count {
            samphone.signal()
        }
        samphone.wait()
        return images
        
        
    }
    class func imageFileName<T>(dataType:T?)->String?{
        if let url = dataType as? URL {
            return url.lastPathComponent
        }else if let data = dataType as? Data{
            return data.fileName
        }else if let image = dataType as? UIImage{
            return image.fileName
        }else{
            return nil
        }
    }
    
    
    class func fetchBatchImages<T>(using dataTypes: [T],
                                   
                                   partialFetchHandler: @escaping (_ imageData: AFSImage?, _ index: Int) -> Void,
                                   completion: @escaping () -> Void){
        
        performBatchImageFetching(using: dataTypes, currentImageIndex: 0, partialFetchHandler: { (imageData, index) in
            partialFetchHandler(imageData, index)
        }) {
            completion()
        }
        
    }
    
    //MARK:-  BatchImageFetching USING Data List
    class private func performBatchImageFetching<T>(using dataTypes: [T],
                                                     currentImageIndex: Int,
                                                     partialFetchHandler: @escaping (_ imageData: AFSImage?, _ index: Int) -> Void,
                                                     completion: @escaping () -> Void) {
        
        guard currentImageIndex < dataTypes.count else {
            completion()
            return
        }
        self.fetchlocalImage(from: dataTypes[currentImageIndex]) { imageData in
            
            partialFetchHandler(imageData, currentImageIndex)
            self.performBatchImageFetching(using: dataTypes, currentImageIndex: currentImageIndex + 1, partialFetchHandler: partialFetchHandler, completion: completion)
        }
        
    }
    class func fetchlocalImage<T>(from dataType:T, completion: @escaping (_ imageData: AFSImage?) -> Void){
        DispatchQueue.global(qos: .background).async {
            guard let filename  = self.imageFileName(dataType: dataType) else{return}
            if let image = self.getImage(filename: filename){
                completion(image)
            }else{
                let res =  self.write(dataType: dataType)
                if res.isWrite {
                    if let image = self.getImage(filename: res.fileName){
                        completion(image)
                    }else{
                        completion(nil)
                    }
                }
            }
            
            
            
            
            
            
        }
    }
    
    //MARK: - deletefileFromDirectory-
    class func deletefile(filename:String)-> Bool{
        if let url = self.fileURL(filename: filename){
            if self.default.isDeletableFile(atPath: url.path) {
                do{
                    try self.default.removeItem(at: url)
                    return true
                }catch {
                    return false
                }
            }else{
                return false
            }
            
        }
        return false
        
        
        
        
    }
   class func deleteBatchImages(using dataTypes:[String]) {
        DispatchQueue.global().async {
            dataTypes.forEach({_ = self.deletefile(filename: $0)})
            
        }
    }
    
}
var kAppTitle:String {return Bundle.kAppTitle}
var kAppIcon:UIImage?{return Bundle.kAppIcon}
