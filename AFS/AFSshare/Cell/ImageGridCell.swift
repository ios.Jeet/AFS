//
//  ImageGridCell.swift
//  AFSshare
//
//  Created by Jitendra Kumar on 09/07/20.
//  Copyright © 2020 Harsh Rajput. All rights reserved.
//

import UIKit

class ImageGridCell: UICollectionViewCell {
    @IBOutlet fileprivate weak var imageView:JKImageView!
    @IBOutlet fileprivate weak var shadowView: JKCardView!
    @IBOutlet fileprivate weak var progressBar: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.shadowView.isHidden = true
        shadowView.cornerRadius = imageView.cornerRadius
        shadowView.masksToBounds = imageView.masksToBounds
        shadowView.clipsToBound = imageView.clipsToBound
    }
    var image:UIImage?{
        didSet{
            imageView.image = image
        }
    }
    var fileURL:URL?{
        didSet{
            DispatchQueue.global(qos: .default).async {
                if let url = self.fileURL {
                    self.imageView.image = UIImage(contentsOfFile: url.path)
                           
                }
            }
           
        }
    }
    var progress:Double = 0.0{
        didSet{
            UIView.animate(withDuration: 0.4) {
                self.shadowView.isHidden = self.progress == 0.0
            }
            //imageView.borderColor = progress == 0.0 ? .clear:.systemBlue
            self.progressBar.progress = Float(progress)
        }
    }
}
