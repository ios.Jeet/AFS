//
//  InfoController.swift
//  AFS
//
//  Created by Harsh Rajput on 03/07/20.
//  Copyright © 2020 Harsh Rajput. All rights reserved.
//

import UIKit
import WebKit
import AFS
class InfoController: UIViewController {
    @IBOutlet private weak var webView: WKWebView!
    @IBOutlet private weak var indicatorView:UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //www.fruitscout.ai
        if #available(iOS 13, *) {
            indicatorView.style = .medium
        }else{
            indicatorView.style = .gray
        }
        webView.navigationDelegate = self
        loadWebView()
    }
   private func loadWebView(){
        if let url  = URL(string: "https://www.fruitscout.ai") {
            webView.load(.init(url:url))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    
}
extension InfoController:WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.indicatorView.startAnimating()
        print("Started to load")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished loading")
        self.indicatorView.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
        self.indicatorView.stopAnimating()
    }
}

